import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BodyComponent } from './body/body.component';
import { HeaderComponent} from './header/header.component';
import { ChiamateComponent } from './chiamate/chiamate.component';
import { DirettiveComponent } from './direttive/direttive.component';
import { BindComponent } from './bind/bind.component';
import { FormsComponent } from './forms/forms.component';
import { VarieComponent } from './varie/varie.component';
import { NgClassComponent } from './direttive/ng-class/ng-class.component';
import { OnewayComponent } from './bind/oneway/oneway.component';
import { TwowayComponent } from './bind/twoway/twoway.component';


const routes: Routes = [
 { path: '', component: BodyComponent, pathMatch: 'full'},
 { path: 'Header', component: HeaderComponent},
 { path: 'Direttive', component: DirettiveComponent},
 { path: 'Chiamate', component: ChiamateComponent},
 { path: 'Binding', component: BindComponent},
 { path: 'Forms', component: FormsComponent},
 { path: 'Varie', component: VarieComponent},
 { path: 'Oneway', component: OnewayComponent},
 { path: 'Twoway', component: TwowayComponent},
 { path: 'NgClass', component: NgClassComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
