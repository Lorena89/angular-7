import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VarieComponent } from './varie.component';

describe('VarieComponent', () => {
  let component: VarieComponent;
  let fixture: ComponentFixture<VarieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VarieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VarieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
