import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-oneway',
  templateUrl: './oneway.component.html',
  styleUrls: ['./oneway.component.scss']
})
export class OnewayComponent implements OnInit {
  nome="Lorena";
  urlSito = window.location.href;
  successClass="text-success";
  hasError=false;
  isSpecial=true;
  highlightColor="orange";
  evento="";
  
  messageClasses={
    "text-success":!this.hasError,
    "text-special":this.isSpecial,
    "text-danger": this.hasError
    }
  titleStyles = {
    color:"pink",
    fontStyle: "italic"

  }
    constructor() { }
  
    ngOnInit() {
    }
  Io(){
    return "Ciao " + this.nome;
  }
  onClick(){
    console.log('collegamento all evento click');
  }
  onClick2(event){
    this.evento="collegamento con string interpolation dal templeate al modello tramite click";
 console.log(event.type); /* stampa in console il tipo di evento */
  
  }
  logMessage(value){
    console.log(value); /*prende il valore che si immette e lo si visualizza in console*/
  }
  }
  