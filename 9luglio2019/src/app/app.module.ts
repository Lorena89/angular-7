import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BodyComponent } from './body/body.component';
import { ChiamateComponent } from './chiamate/chiamate.component';
import { DirettiveComponent } from './direttive/direttive.component';
import { BindComponent } from './bind/bind.component';
import { FormsComponent } from './forms/forms.component';
import { VarieComponent } from './varie/varie.component';
import { NgClassComponent } from './direttive/ng-class/ng-class.component';
import { OnewayComponent } from './bind/oneway/oneway.component';
import { TwowayComponent } from './bind/twoway/twoway.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    BodyComponent,
    ChiamateComponent,
    DirettiveComponent,
    BindComponent,
    FormsComponent,
    VarieComponent,
    NgClassComponent,
    OnewayComponent,
    TwowayComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
